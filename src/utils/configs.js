export const config = {
	//baseUrl: "http://175.107.63.31:5000/api/",
	baseUrl: "http://localhost:57554/api/",
	getToken: "Account/Token",
	getUserMozas: "Moza",
	saveProgress: "Progress",
	updateProgress: "Progress/",
	getProgressWithId: "Progress/",
	getUnApprovedSummaries: "Progress/GetUnApproved",
	getRejectedSummaries: "Progress/GetRejected",
	approveProgress: "Progress/Approve",
	rejectProgress: "Progress/Reject",
	saveVisit: "Visit/SOVisit",
	localStorageKey: "token",
	mansehraReport: "https://app.powerbi.com/view?r=eyJrIjoiZDU4Nzg0YjctNTVlNC00NGQ5LTg2ZjQtYjRlZTA1YmJkOTlhIiwidCI6IjgzZDIzYTU0LTk1NjAtNGFkMy1iYjAzLTY2YWU2NjNkOWNiZiIsImMiOjl9",
	abbotabadReport: "https://app.powerbi.com/view?r=eyJrIjoiMWFlZDQ3MzAtOTBhMS00MDkyLTgzOWEtYTMwZjM0YjU5NmQwIiwidCI6IjgzZDIzYTU0LTk1NjAtNGFkMy1iYjAzLTY2YWU2NjNkOWNiZiIsImMiOjl9",
	chitralReport: "https://app.powerbi.com/view?r=eyJrIjoiMTE3NzZkMTktYmYzZi00NjRkLWI4ZDMtOWYyMzQ0N2FjZTBkIiwidCI6IjgzZDIzYTU0LTk1NjAtNGFkMy1iYjAzLTY2YWU2NjNkOWNiZiIsImMiOjl9",
	nowshehraReport: "https://app.powerbi.com/view?r=eyJrIjoiYmFmMDEwODctNTE2Yy00YTg2LWEzZWYtNzE1NTAxNmY5OWYxIiwidCI6IjgzZDIzYTU0LTk1NjAtNGFkMy1iYjAzLTY2YWU2NjNkOWNiZiIsImMiOjl9"
};