import React from 'react';
import Navigation from './navigation';
import Main from './main';

export default class App extends React.Component {

  render() {
    return (
      <div className="nav-full-height">
        <Navigation />
        <Main />
      </div>
    )
  }
}
