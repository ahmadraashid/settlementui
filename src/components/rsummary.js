import React from 'react';
import Navigation from './navigation';
import DataService from '../services/data_service';
import ReactTable from 'react-table';
import PropTypes from 'prop-types';
import "react-table/react-table.css";


const dataService = new DataService();
export default class RSummary extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    }

    constructor(props, context) {
        super(props, context);
        this.state = {
            dataLoading: '',
            canEdit: '',
            editSummary: false,
            summaryData: [],
            selected: {}, selectAll: 0,
            dataColumns: [{
                columns: [
                    {
                        Header: "Edit",
                        id: "btn",
                        accessor: "id",
                        Cell: ({ original }) => {
                            return (
                                <button
                                    type="button"
                                    className={"btn btn-primary btn-sm " + this.state.canEdit}
                                    id={original.id}
                                    onClick={() => this.editSummary(original.id)}
                                >Edit</button>
                            );
                        },
                        sortable: false,
                        width: 60
                    },
                    {
                        Header: "Month",
                        accessor: "month",
                        width: 60
                    },
                    {
                        Header: "Year",
                        accessor: "year",
                        width: 60
                    },
                    {
                        Header: "Moza",
                        accessor: "moza",
                    },
                    {
                        Header: "Shaj",
                        accessor: "shajraNasbWriting",
                        width: 60
                    },
                    {
                        Header: "Aks",
                        accessor: "aksPatangi",
                        width: 60
                    },
                    {
                        Header: "Khat",
                        accessor: "khatooniWriting",
                        width: 60
                    },
                    {
                        Header: "Khass",
                        accessor: "khassraMeasurement",
                        width: 60
                    },
                    {
                        Header: "Field",
                        accessor: "fieldBook",
                        width: 60
                    },
                    {
                        Header: "Misle",
                        accessor: "misleHaqiyat",
                        width: 60
                    },
                    {
                        Header: "Maus",
                        accessor: "mausaavis",
                        width: 60
                    },
                    {
                        Header: "Aks",
                        accessor: "aksPatangi",
                        width: 60
                    },
                    {
                        Header: "Kisht",
                        accessor: "shajraKishtwaar",
                        width: 60
                    },
                    ,
                    {
                        Header: "Description",
                        width: 80,
                        accessor: 'description',
                        id: 'click-me-button',
                        Cell: row => (<button className="btn btn-info btn-sm" onClick={(e) => this.handleDescriptionView(row.value)}>View</button>),
                    }
                ]
            }]
        };

        this.handlePageChange = this.handlePageChange.bind(this);
        this.loadSummaries = this.loadSummaries.bind(this);
        this.editSummary = this.editSummary.bind(this);
        this.handleDescriptionView = this.handleDescriptionView.bind(this);
    }

    componentDidMount() {
        var roles = localStorage.getItem('roles');
        if (roles.indexOf("Admin") === -1 && roles.indexOf("SO") === -1) {
            this.setState(() => ({
                approveSummaryState: 'disabled no-events',
                rejectSummaryState: 'disabled no-events'
            }));
        } else if (roles.indexOf("Admin") === -1 && roles.indexOf("Operator") === -1) {
            this.setState(() => ({
                canEdit: 'disabled no-events'
            }));
        }
        this.loadSummaries();
    }

    editSummary(id) {
        localStorage.setItem("sid", id);
        this.setState(() => ({
            editSummary: true
        }));
        this.context.router.history.push('/uprogress/' + id);
    }

    handlePageChange() {

    }

    handleDescriptionView(data) {
        alert(data);
    }

    loadSummaries() {
        dataService.getRejectedSummaries()
            .then((json) => {
                if (json && json.length > 0) {
                    this.setState(() => ({
                        summaryData: json,
                        dataLoading: 'hidden'
                    }));
                } else {
                    this.setState(() => ({
                        dataLoading: 'hidden'
                    }));
                }
            })
            .catch((error) => {
                this.setState(() => ({
                    dataLoading: 'hidden'
                }));
                console.error(error);
            });
    }


    render() {
        return (
            <div className="nav-full-height">
                <Navigation />
                <div className="app-content">
                    <nav className="navbar navbar-expand navbar-light bg-white">
                        <button type="button" className="btn btn-sidebar" data-toggle="sidebar">
                            <i className="fas fa-bars"></i>
                        </button>
                        <div className="navbar-brand">
                            Rejected Summary List
                    </div>
                    </nav>
                    <div className="row">
                        <div className="col-md-6">
                            <img src="/assets/images/loading.gif" alt="Loading Activity" className={"loader " + this.state.dataLoading} />
                            <span className={"loader-text " + this.state.dataLoading}>Wait loading data...</span>
                        </div>
                        <div className="col-md-6 text-right">
                        </div>
                    </div>
                    <div className="container">
                        <div className="h-100 margin-top-10" id="header">
                            <ReactTable className="table"
                                columns={this.state.dataColumns}
                                defaultPageSize={10}
                                className="-striped -highlight"
                                data={this.state.summaryData}
                            />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}