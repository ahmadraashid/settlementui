import React from 'react';
import Navigation from './navigation';

export default class Home extends React.Component {

    render() {
        return (
            <div className="nav-full-height">
                <Navigation />

                <div className="app-content">
                    <nav className="navbar navbar-expand navbar-light bg-white">
                        <button type="button" className="btn btn-sidebar" data-toggle="sidebar">
                            <i className="fas fa-bars"></i>
                        </button>
                        <div className="navbar-brand">
                            Settlement Application
                </div>
                    </nav>
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item active" aria-current="page">Introduction</li>
                        </ol>
                    </nav>
                    <div className="container-fluid">
                        <h2>
                            Welcome to Settlement Application, Board of Revenue, Govt of KPK.
                        </h2>
                    </div>
                </div>
            </div>
        )

    }
}