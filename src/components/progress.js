import React from 'react';
import Navigation from './navigation';
import DataService from '../services/data_service';

const dataService = new DataService();
export default class Progress extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            'khatooniWriting': 0,
            'openModal': false,
            'modalMessage': '',
            'modalTitle': '',
            'mozaId': 0,
            'year': 0,
            'month': 0,
            'shajraNasbWriting': 0,
            'aksPatangi': 0,
            'khassraMeasurement': 0,
            'fieldBook': 0,
            'misleHaqiyat': 0,
            'mausaavis': 0,
            'shajraKishtwaar': 0,
            'description': '',
            'mozasList': [],
            'monthsList': [{
                'id': 0,
                'name': '--Select Month--'
            }, {
                'id': 1,
                'name': 'January'
            }, {
                'id': 2,
                'name': 'February'
            }, {
                'id': 3,
                'name': 'March'
            }, {
                'id': 4,
                'name': 'April'
            }, {
                'id': 5,
                'name': 'May'
            }, {
                'id': 6,
                'name': 'June'
            }, {
                'id': 7,
                'name': 'July'
            }, {
                'id': 8,
                'name': 'August'
            }, {
                'id': 9,
                'name': 'September'
            }, {
                'id': 10,
                'name': 'October'
            }, {
                'id': 11,
                'name': 'November'
            }, {
                'id': 12,
                'name': 'December'
            }
            ],
            'processing': '',
            'bText': 'Save Summary',
            'loading': ''
        };

        this.mozaChanged = this.mozaChanged.bind(this);
        this.yearChanged = this.yearChanged.bind(this);
        this.monthChanged = this.monthChanged.bind(this);
        this.loadUserMozas = this.loadUserMozas.bind(this);

        this.handleKhatooniWriting = this.handleKhatooniWriting.bind(this); 
        this.handleShajraWriting = this.handleShajraWriting.bind(this);
        this.handleAksPatangi = this.handleAksPatangi.bind(this);
        this.handleKhasraMeasurement = this.handleKhasraMeasurement.bind(this);
        this.handleFieldBook = this.handleFieldBook.bind(this);
        this.handleMisleHaqiyat = this.handleMisleHaqiyat.bind(this);
        this.handleMausaavis = this.handleMausaavis.bind(this);
        this.handleShajraKishtwaar = this.handleShajraKishtwaar.bind(this);
        this.handleDescription = this.handleDescription.bind(this);
        this.saveMonthlyProgress = this.saveMonthlyProgress.bind(this);
    }

    componentDidUpdate() {
    }

    componentDidMount() {
        this.loadUserMozas();
    }

    monthChanged(e) {
        var index = e.target.selectedIndex;
        var monthValue = this.state.monthsList[index].id;
        this.setState({
            month: monthValue
        });
    }

    handleKhatooniWriting(e) {
        if (!isNaN(e.target.value)) {
            this.setState({
                khatooniWriting: e.target.value
            });
        }
    }

    handleShajraWriting(e) {
        if (!isNaN(e.target.value)) {
            this.setState({
                shajraNasbWriting: e.target.value
            });
        }
    }

    handleAksPatangi(e) {
        if (!isNaN(e.target.value)) {
            this.setState({
                aksPatangi: e.target.value
            });
        }
    }

    handleKhasraMeasurement(e) {
        if (!isNaN(e.target.value)) {
            this.setState({
                khassraMeasurement: e.target.value
            });
        }
    }

    handleFieldBook(e) {
        if (!isNaN(e.target.value)) {
            this.setState({
                fieldBook: e.target.value
            });
        }
    }

    handleMisleHaqiyat(e) {
        if (!isNaN(e.target.value)) {
            this.setState({
                misleHaqiyat: e.target.value
            });
        }
    }

    handleMausaavis(e) {
        if (!isNaN(e.target.value)) {
            this.setState({
                mausaavis: e.target.value
            });
        }
    }

    handleShajraKishtwaar(e) {
        if (!isNaN(e.target.value)) {
            this.setState({
                shajraKishtwaar: e.target.value
            });
        }
    }

    handleDescription(e) {
        this.setState({
            description: e.target.value
        });
    }

    mozaChanged(e) {
        var index = e.target.selectedIndex;
        if (index > 0)
            --index;
            
        var selectedMoza = this.state.mozasList[index].id;
        this.setState({
            mozaId: selectedMoza
        });
    }

    yearChanged(e) {
        var index = e.target.selectedIndex;
        var selectedYear = e.target[index].value;
        this.setState({
            year: selectedYear
        });
    }

    loadUserMozas() {
        dataService.getMozas()
            .then((mozas) => {
                if (mozas && mozas.length > 0) {
                    this.setState(() => ({
                        mozasList: mozas,
                        loading: ''
                    }));
                }
            })
            .catch((error) => {
                console.error(error);
                this.setState(() => ({
                    loading: ''
                }));
            });

    }

    saveMonthlyProgress() {
        if (this.state.mozaId === 0 ||
            this.state.month === 0 ||
            this.state.year === 0) {
                alert('The data is not valid to be submitted');
                return false;
            }

        var progress = {
            'Year': this.state.year,
            'Month': this.state.month,
            'MozaId': this.state.mozaId,
            'KhatooniWriting': this.state.khatooniWriting,
            'ShajraNasbWriting': this.state.shajraNasbWriting,
            'AksPatangi': this.state.aksPatangi,
            'KhassraMeasurement': this.state.khassraMeasurement,
            'FieldBook': this.state.fieldBook,
            'MisleHaqiyat': this.state.misleHaqiyat,
            'Mausaavis': this.state.mausaavis,
            'ShajraKishtwaar': this.state.shajraKishtwaar,
            'Description': this.state.description
        }

        this.setState(() => ({
            'processing': 'disabled',
            'bText': 'Submitting Summary...'
        }));

        dataService.saveProgress(progress)
            .then((response) => {
                alert('Progress saved Successfully');

                this.setState(() => ({
                    'khatooniWriting': 0,
                    'shajraNasbWriting': 0,
                    'aksPatangi': 0,
                    'khassraMeasurement': 0,
                    'fieldBook': 0,
                    'misleHaqiyat': 0,
                    'mausaavis': 0,
                    'shajraKishtwaar': 0,
                    'description': '',
                    'bText': 'Save Summary',
                    'processing': '',
                    'loading': 'disabled'
                }));
            })
            .catch((error) => {
                alert('Something went wrong');
                this.setState(() => ({
                    'processing': '',
                    'bText': 'Save Summary'
                }));
            });
    }

    /*openModal = () => {
        this.setState({ openModal: true });
    };
    closeModal = () => {
        this.setState({ openModal: false });
    };*/

    render() {
        var yearsList = [];
        var currentYear = new Date().getFullYear();
        for (var y = 2010; y <= currentYear; y++) {
            yearsList.push(y);
        }
        let years = yearsList.map((year) =>
            <option key={year}>{year}</option>
        );

        let months = this.state.monthsList;
        let monthNames = months.map((month) =>
            <option key={month.id}>{month.name}</option>
        );

        let mozas = this.state.mozasList;
        let mozasOptions = mozas.map((moza) =>
            <option key={moza.id}>{moza.name}</option>
        );

        return (
            <div className="nav-full-height">
                <Navigation />
                <div className="app-content">
                    <nav className="navbar navbar-expand navbar-light bg-white">
                        <button type="button" className="btn btn-sidebar" data-toggle="sidebar">
                            <i className="fas fa-bars"></i>
                        </button>
                        <div className="navbar-brand">
                            Monthly Report Form
                    </div>
                    </nav>
                    <br />
                    <div className="container">
                        <div className="h-100" id="header">
                            <form>
                                <div className="form-group col-md-8">
                                    <div className="row">
                                        <label className="col-md-4">Moza: </label>
                                        <select onChange={this.mozaChanged} className={"form-control col-md-8 " + this.state.loading }>
                                            <option value=''>--Select Moza--</option>
                                            {mozasOptions}
                                        </select>
                                    </div>

                                </div>
                                <div className="form-group col-md-8">
                                    <div className="row">
                                        <label className="col-md-4">Year: </label>
                                        <select onChange={this.yearChanged} className="form-control col-md-8">
                                            <option val=''>--Select Year--</option>
                                            {years}
                                        </select>
                                    </div>
                                </div>
                                <div className="form-group col-md-8">
                                    <div className="row">
                                        <label className="col-md-4">Month: </label>
                                        <select onChange={this.monthChanged} className="form-control col-md-8">
                                            {monthNames}
                                        </select>
                                    </div>
                                </div>
                                <div className="form-group col-md-8">
                                    <div className="row">
                                        <label className="col-md-4">Khatooni Writing: </label>
                                        <input value={this.state.khatooniWriting} onChange={this.handleKhatooniWriting} className="form-control col-md-8" placeholder="Shajra Writing" />
                                    </div>
                                </div>
                                <div className="form-group col-md-8">
                                    <div className="row">
                                        <label className="col-md-4">Shajra Writing: </label>
                                        <input value={this.state.shajraNasbWriting} onChange={this.handleShajraWriting} className="form-control col-md-8" placeholder="Shajra Writing" />
                                    </div>
                                </div>
                                <div className="form-group col-md-8">
                                    <div className="row">
                                        <label className="col-md-4">Aks Patangi: </label>
                                        <input value={this.state.aksPatangi} onChange={this.handleAksPatangi} className="form-control col-md-8" maxLength="9" placeholder="Aks Patangi" />
                                    </div>
                                </div>
                                <div className="form-group col-md-8">
                                    <div className="row">
                                        <label className="col-md-4">Khassra Measurement: </label>
                                        <input value={this.state.khassraMeasurement} onChange={this.handleKhasraMeasurement} className="form-control col-md-8" maxLength="9" placeholder="khassra Measurement" />
                                    </div>
                                </div>
                                <div className="form-group col-md-8">
                                    <div className="row">
                                        <label className="col-md-4">Field Book: </label>
                                        <input value={this.state.fieldBook} onChange={this.handleFieldBook} className="form-control col-md-8" maxLength="9" placeholder="Field Book" />
                                    </div>
                                </div>
                                <div className="form-group col-md-8">
                                    <div className="row">
                                        <label className="col-md-4">Misle Haqiyat: </label>
                                        <input value={this.state.misleHaqiyat} onChange={this.handleMisleHaqiyat} className="form-control col-md-8" maxLength="9" placeholder="Misle Haqiyat" />
                                    </div>
                                </div>
                                <div className="form-group col-md-8">
                                    <div className="row">
                                        <label className="col-md-4">Mausaavis: </label>
                                        <input value={this.state.mausaavis} onChange={this.handleMausaavis} className="form-control col-md-8" maxLength="9" placeholder="Mausaavis" />
                                    </div>
                                </div>
                                <div className="form-group col-md-8">
                                    <div className="row">
                                        <label className="col-md-4">Shajra Kishtwaar: </label>
                                        <input value={this.state.shajraKishtwaar} onChange={this.handleShajraKishtwaar} className="form-control col-md-8" maxLength="9" placeholder="Shajra Kishtwaar" />
                                    </div>
                                </div>
                                <div className="form-group col-md-8">
                                    <div className="row">
                                        <label className="col-md-4">Description: </label>
                                        <textarea rows="5" value={this.state.description} placeholder="Description" onChange={this.handleDescription} className="form-control col-md-8" maxLength="250">
                                        </textarea>
                                    </div>
                                </div>
                                <div className="form-group col-md-8">
                                    <div className="row">
                                        <button onClick={this.saveMonthlyProgress} className={"btn btn-info btn-lg btn-block " + this.state.processing}  type="button">{this.state.bText}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}