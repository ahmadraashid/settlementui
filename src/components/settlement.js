import React from 'react';
import Navigation from './navigation';
import { config } from '../utils/configs';

export default class Settlement extends React.Component {

    pageCounter = 0;
    constructor(props) {
        super(props);

        this.state = { "district": "", "url": "", "pageRefreshCounter": 0, "currentDistrict": "" };
        this.setDistrictReport = this.setDistrictReport.bind(this);
    }

    componentDidUpdate() {
        ++this.pageCounter;
        if (this.pageCounter > 1) {
            this.setDistrictReport();
        }
    }

    componentDidMount() {
        this.setDistrictReport();
    }

    setDistrictReport() {
        var pathName = window.location.pathname;
        var district = pathName.split('/')[2];

        if (this.state.currentDistrict !== district) {
            switch (district) {
                case "m":
                    this.setState(() => ({
                        url: config.mansehraReport,
                        currentDistrict: district
                    }));
                    break;

                case "a":
                    this.setState(() => ({
                        url: config.abbotabadReport,
                        currentDistrict: district
                    }));
                    break;

                case "n":
                    this.setState(() => ({
                        url: config.nowshehraReport,
                        currentDistrict: district
                    }));
                    break;

                case "c":
                    this.setState(() => ({
                        url: config.chitralReport,
                        currentDistrict: district
                    }));
                    break;
            }
        }
    }

    render() {
        return (
            <div className="nav-full-height">
                <Navigation />

                <div className="app-content">
                    <nav className="navbar navbar-expand navbar-light bg-white">
                        <button type="button" className="btn btn-sidebar" data-toggle="sidebar">
                            <i className="fas fa-bars"></i>
                        </button>
                        <div className="navbar-brand">
                            Mansehra Settlement
                </div>
                    </nav>
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item active" aria-current="page">Introduction</li>
                        </ol>
                    </nav>
                    <div className="container-fluid">
                        <iframe title="Mansehra Settlement Report"
                            width="1024" height="800"
                            src={this.state.url} frameBorder="0" allowFullScreen="true"></iframe>
                    </div>
                </div>
            </div>
        )

    }
}