import React from 'react';
import DataService from '../services/data_service';
//import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';


export default class Login extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    }

    constructor(props, context) {
        super(props, context);
        this.state = {
            'username': '', 'password': '',
            'redirect': false, 'processing': '',
            'bText': 'Login',
            'isError': 'hidden',
            'mounted': false
        };

        this.authenticateLogin = this.authenticateLogin.bind(this);
        this.handleUserName = this.handleUserName.bind(this);
        this.handlePassword = this.handlePassword.bind(this);
    }

    componentDidMount() {
        this.setState(() => ({
            mounted: true
        }));
    }

    componentWillMount() {
        this.setState(() => ({
            mounted: false
        }));
    }

    handleUserName(event) {
        if (this.state.mounted) {
            this.setState({ 'username': event.target.value });
        }
    }

    handlePassword(event) {
        if (this.state.mounted) {
            this.setState({ 'password': event.target.value });
        }
    }

    authenticateLogin() {
        if (this.state.mounted) {
            this.setState(() => ({
                'processing': 'disabled',
                'bText': 'Wait Processing...',
                'isError': 'hidden'
            }));
            const dataService = new DataService();
            dataService.authenticate(this.state.username, this.state.password)
                .then((json) => {
                    if (json && json.token) {
                        localStorage.setItem('token', json.token);
                        localStorage.setItem('roles', json.roles);
                        localStorage.setItem('fullName', json.fullName);
                        localStorage.setItem('district', json.district);
                        /*this.setState(() => ({
                            redirect: true
                        }));*/
                        this.context.router.history.push('/home');
                    }
                    this.setState(() => ({
                        'bText': 'Login',
                        'processing': '',
                        'isError': 'hidden'
                    }));
                })
                .catch((error) => {
                    this.setState(() => ({
                        'bText': 'Login',
                        'processing': '',
                        'isError': ''
                    }));
                    console.error(error);
                });
        }
    }

    render() {
        // if (this.state.redirect) {
        //     return <Redirect to='/home' />;
        // } else {
            return (
                <div className="app-content">
                    <nav className="navbar navbar-expand navbar-light bg-white">
                        <button type="button" className="btn btn-sidebar" data-toggle="sidebar">
                            <i className="fas fa-bars"></i>
                        </button>
                        <div className="navbar-brand">
                            Settlement Application, PMU BOR, Govt of KPK.
                </div>
                    </nav>
                    <div className="container-fluid">
                        <div className="d-flex align-items-center flex-column justify-content-center h-100 margin-top-login-box" id="header">
                            <div className={"alert alert-danger alert-dismissible fade show " + this.state.isError}>
                                <button type="button" className="close" data-dismiss="alert">&times;</button>
                                <strong>Error!</strong> Username/Password provided is wrong
                        </div>
                            <h3 className="display-6">Enter your Login Information</h3>
                            <form>
                                <div className="form-group">
                                    <input value={this.state.username} onChange={this.handleUserName} className="form-control form-control-lg" placeholder="Username" type="text" maxLength="20" required />
                                </div>
                                <div className="form-group">
                                    <input value={this.state.password} onChange={this.handlePassword} className="form-control form-control-lg" placeholder="Password" type="password" maxLength="20" required />
                                </div>
                                <div className="form-group">
                                    <button type="button" onClick={this.authenticateLogin} className={"btn btn-info btn-lg btn-block " + this.state.processing}>{this.state.bText}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            )
        }
    //}
}