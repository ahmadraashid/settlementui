import React from 'react';
import Navigation from './navigation';
import DataService from '../services/data_service';

const dataService = new DataService();
export default class Monitoring extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            mozasList: [],
            remarks: '',
            bText: 'Save Visit',
            processing: ''
        };

        this.handleDescription = this.handleDescription.bind(this);
    }

    handleDescription(e) {
        this.setState(() => {
            remarks = e.target.value;
        });
    }

    saveVisit() {
        if (this.state.mozaId === 0) {
                alert('Select a Moza');
                return false;
            }

        var visit = {
            'MozaId': this.state.mozaId,
            'Remarks': this.state.remarks
        }

        this.setState(() => ({
            'processing': 'disabled',
            'bText': 'Submitting Visit...'
        }));

        dataService.saveVisit(visit)
            .then((response) => {
                alert('Visit saved Successfully');

                this.setState(() => ({
                    'bText': 'Save Visit',
                    'processing': '',
                    'loading': 'disabled'
                }));
            })
            .catch((error) => {
                alert('Something went wrong');
                this.setState(() => ({
                    'processing': '',
                    'bText': 'Save Summary'
                }));
            });
    }

    render() {
        return (
            <div className="nav-full-height">
                <div className="app-content">
                    <nav className="navbar navbar-expand navbar-light bg-white">
                        <button type="button" className="btn btn-sidebar" data-toggle="sidebar">
                            <i className="fas fa-bars"></i>
                        </button>
                        <div className="navbar-brand">
                            Monitoring Visits
                    </div>
                    </nav>
                    <br />
                    <div className="container">
                        <div className="h-100" id="header">
                            <form>
                                <div className="form-group col-md-8">
                                    <div className="row">
                                        <label className="col-md-4">Moza: </label>
                                        <select onChange={this.mozaChanged} className={"form-control col-md-8 " + this.state.loading }>
                                            <option value=''>--Select Moza--</option>
                                            {mozasOptions}
                                        </select>
                                    </div>
                                </div>
                                <div className="form-group col-md-8">
                                    <div className="row">
                                        <label className="col-md-4">Remarks: </label>
                                        <textarea rows="5" value={this.state.remarks} placeholder="Description" onChange={this.handleDescription} className="form-control col-md-8" maxLength="250">
                                        </textarea>
                                    </div>
                                </div>
                                <div className="form-group col-md-8">
                                    <div className="row">
                                        <button onClick={this.saveVisit} className={"btn btn-info btn-lg btn-block " + this.state.processing}  type="button">{this.state.bText}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}