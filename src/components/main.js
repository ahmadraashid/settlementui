import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from './home';
import Progress from './progress';

// The Main component renders one of the three provided
// Routes (provided that one matches). Both the /roster
// and /schedule routes will match any pathname that starts
// with /roster or /schedule. The / route will only match
// when the pathname is exactly the string "/"
export default class Main extends React.Component {
  render() {
    return (
      <Switch>
        <Route path='/home' component={Home} />
        <Route path='/progress/:id' component={Progress} />
      </Switch>
    )
  }
}