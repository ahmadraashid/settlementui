import React from 'react';
import Navigation from './navigation';
import DataService from '../services/data_service';
import ReactTable from 'react-table';
import "react-table/react-table.css";

const dataService = new DataService();
export default class USummary extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            doneList: [],
            dataLoading: '',
            approveSummaryState: '',
            approveSummaryText: 'Approve Summaries',
            rejectSummaryState: '',
            rejectSummaryText: 'Reject Summaries',
            summaryData: [],
            selected: {}, selectAll: 0,
            dataColumns: [{
                columns: [
                    {
                        id: "checkbox",
                        accessor: "",
                        Cell: ({ original }) => {
                            return (
                                <input
                                    type="checkbox"
                                    className="checkbox"
                                    id={original.id}
                                    checked={this.state.selected[original.id] === true}
                                    onChange={() => this.toggleRow(original.id)}
                                />
                            );
                        },
                        Header: x => {
                            return (
                                <input
                                    type="checkbox"
                                    className="checkbox"
                                    checked={this.state.selectAll === 1}
                                    ref={input => {
                                        if (input) {
                                            input.indeterminate = this.state.selectAll === 2;
                                        }
                                    }}
                                    onChange={() => this.toggleSelectAll()}
                                />
                            );
                        },
                        sortable: false,
                        width: 45
                    },
                    {
                        Header: "#",
                        accessor: "id",
                        width: 25
                    },
                    {
                        Header: "Month",
                        accessor: "month",
                        width: 60
                    },
                    {
                        Header: "Year",
                        accessor: "year",
                        width: 60
                    },
                    {
                        Header: "Moza",
                        accessor: "moza",
                    },
                    {
                        Header: "Shaj",
                        accessor: "shajraNasbWriting",
                        width: 60
                    },
                    {
                        Header: "Aks",
                        accessor: "aksPatangi",
                        width: 60
                    },
                    {
                        Header: "Khat",
                        accessor: "khatooniWriting",
                        width: 60
                    },
                    {
                        Header: "Khass",
                        accessor: "khassraMeasurement",
                        width: 60
                    },
                    {
                        Header: "Field",
                        accessor: "fieldBook",
                        width: 60
                    },
                    {
                        Header: "Misle",
                        accessor: "misleHaqiyat",
                        width: 60
                    },
                    {
                        Header: "Maus",
                        accessor: "mausaavis",
                        width: 60
                    },
                    {
                        Header: "Aks",
                        accessor: "aksPatangi",
                        width: 60
                    },
                    {
                        Header: "Kisht",
                        accessor: "shajraKishtwaar",
                        width: 60
                    },
                    {
                        Header: "Description",
                        width: 80,
                        accessor: 'description',
                        id: 'click-me-button',
                        Cell: row => (<button className="btn btn-info btn-sm" onClick={(e) => this.handleDescriptionView(row.value)}>View</button>),
                    }
                ]
            }]
        };

        this.loadSummaries = this.loadSummaries.bind(this);
        this.toggleRow = this.toggleRow.bind(this);
        this.toggleSelectAll = this.toggleSelectAll.bind(this);
        this.approveSummary = this.approveSummary.bind(this);
        this.rejectSummary = this.rejectSummary.bind(this);
        this.handleDescriptionView = this.handleDescriptionView.bind(this);
    }

    componentDidMount() {
        var roles = localStorage.getItem('roles');
        if (roles.indexOf("Admin") === -1 && roles.indexOf("SO") === -1) {
            this.setState(() => ({
                approveSummaryState: 'disabled no-events',
                rejectSummaryState: 'disabled no-events'
            }));
        }

        this.loadSummaries();
    }

    toggleRow(id) {
        const newSelected = Object.assign({}, this.state.selected);
        newSelected[id] = !this.state.selected[id];
        this.setState({
            selected: newSelected,
            selectAll: 2
        });
    }

    toggleSelectAll() {
        let newSelected = {};

        if (this.state.selectAll === 0) {
            this.state.summaryData.forEach(x => {
                newSelected[x.id] = true;
            });
        }

        this.setState({
            selected: newSelected,
            selectAll: this.state.selectAll === 0 ? 1 : 0
        });
    }

    handleDescriptionView(data) {
        alert(data);
    }

    rejectSummary() {
        this.setState(() => ({
            approveSummaryState: 'disabled no-events',
            rejectSummaryState: 'disabled no-events',
            rejectSummaryText: 'Saving Summaries...'
        }));
        var selectedSummaries = this.state.selected;
        var summaryList = [];
        for (var s in selectedSummaries) {
            if (this.state.doneList.indexOf(s) === -1) {
                    summaryList.push(s);
            }
        }

        var updateSummary = {
            "DistrictId": 0,
            "Ids": summaryList
        }

        dataService.rejectSummaryStatus(updateSummary)
            .then((json) => {

                var updatedSummaries = this.state.summaryData.filter(function (summary) {
                    return updateSummary.Ids.indexOf(summary.id.toString()) === -1;
                });

                var newList = this.state.doneList.concat(summaryList);
                this.setState(() => ({
                    summaryData: updatedSummaries,
                    doneList: newList,
                    rejectSummaryText: 'Reject Summaries',
                    rejectSummaryState: '',
                    approveSummaryState: ''
                }));

                alert(summaryList.length + ' summaries rejected successfully');
                summaryList.length = 0;

            }).catch((error) => {
                summaryList.length = 0;
                console.error(error);
            });
    }

    approveSummary() {
        this.setState(() => ({
            approveSummaryState: 'disabled no-events',
            rejectSummaryState: 'disabled no-events',
            approveSummaryText: 'Saving Summaries...'
        }));
        var selectedSummaries = this.state.selected;
        var summaryList = [];
        for (var s in selectedSummaries) {
            if (this.state.doneList.indexOf(s) === -1) {
                summaryList.push(s);
            }
        }
        var updateSummary = {
            "DistrictId": 0,
            "Ids": summaryList
        }

        dataService.approveSummaryStatus(updateSummary)
            .then((json) => {

                var updatedSummaries = this.state.summaryData.filter(function (summary) {
                    return updateSummary.Ids.indexOf(summary.id.toString()) === -1;
                });
                var newList = this.state.doneList.concat(summaryList);
                this.setState(() => ({
                    summaryData: updatedSummaries,
                    doneList: newList,
                    approveSummaryText: 'Approve Summaries',
                    approveSummaryState: '',
                    rejectSummaryState: '',
                    dataLoading: 'hidden'
                }));

                console.log(summaryList);
                alert(summaryList.length + ' summaries approved successfully');

            }).catch((error) => {
                this.setState(() => ({
                    dataLoading: 'hidden'
                }));
                console.error(error);
            });
    }

    loadSummaries() {
        dataService.getUnApprovedSummaries()
            .then((json) => {
                if (json && json.length > 0) {
                    this.setState(() => ({
                        summaryData: json,
                        dataLoading: 'hidden'
                    }));
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }


    render() {
        return (
            <div className="nav-full-height">
                <Navigation />
                <div className="app-content">
                    <nav className="navbar navbar-expand navbar-light bg-white">
                        <button type="button" className="btn btn-sidebar" data-toggle="sidebar">
                            <i className="fas fa-bars"></i>
                        </button>
                        <div className="navbar-brand">
                            Unapproved Summary List
                    </div>
                    </nav>
                    <br />
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6">
                                <img src="/assets/images/loading.gif" alt="Loading Activity" className={"loader " + this.state.dataLoading } />
                                <span className={"loader-text " + this.state.dataLoading}>Wait loading data...</span>
                            </div>
                            <div className="col-md-6 text-right">
                                <button className={"btn btn-info btn-lg " + this.state.approveSummaryState} onClick={this.approveSummary}>{this.state.approveSummaryText}</button>
                                <button className={"btn btn-info btn-lg margin-left-10 " + this.state.rejectSummaryState} onClick={this.rejectSummary} >{this.state.rejectSummaryText}</button>
                            </div>
                        </div>
                        <div className="h-100 margin-top-10" id="header">
                            <ReactTable className="table"
                                columns={this.state.dataColumns}
                                defaultPageSize={10}
                                className="-striped -highlight"
                                data={this.state.summaryData}
                            />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}