import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

export default class Navigation extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    }

    constructor(props, context) {
        super(props, context);
        this.state = {
            'loggedUser': '',
            'mProgress': '',
            'uSummary': '',
            'management': '',
            'reports': '',
            'dChitral': '',
            'dMansehra': '',
            'dAbbotabad': '',
            'dNoshera': ''
        }
        this.logout = this.logout.bind(this);
    }

    componentDidMount() {
        var roles = localStorage.getItem('roles');
        var district = localStorage.getItem('district');
        var loggedUser = localStorage.getItem('fullName');

        this.setState(() => ({
            loggedUser: loggedUser
        }));

        if (roles.indexOf("SO") !== -1) {
            this.setState(() => ({
                mProgress: 'hidden',
                management: 'hidden'
            }));
        } else if (roles.indexOf("Operator") !== -1) {
            this.setState(() => ({
                management: 'hidden',
                reports: 'hidden'
            }));
        }

        if (roles.indexOf("Admin") === 0) {
            //DO NOTHING
        } else {
            switch (district) {
                case 'Abbotabad':
                    this.setState(() => ({
                        dMansehra: 'hidden',
                        dChitral: 'hidden',
                        dNoshera: 'hidden'
                    }));
                    break;

                case 'Mansehra':
                    this.setState(() => ({
                        dAbbotabad: 'hidden',
                        dChitral: 'hidden',
                        dNoshera: 'hidden'
                    }));
                    break;

                case 'Chitral':
                    this.setState(() => ({
                        dMansehra: 'hidden',
                        dAbbotabad: 'hidden',
                        dNoshera: 'hidden'
                    }));
                    break;

                case 'Noshera':
                    this.setState(() => ({
                        dMansehra: 'hidden',
                        dChitral: 'hidden',
                        dAbbotabad: 'hidden'
                    }));
                    break;

                default:
                    this.setState(() => ({
                        dNoshera: 'hidden',
                        dMansehra: 'hidden',
                        dChitral: 'hidden',
                        dAbbotabad: 'hidden'
                    }));
                    break;
            }
        }

    }

    logout(event) {
        event.preventDefault();
        localStorage.removeItem('token');
        localStorage.removeItem('roles');
        localStorage.removeItem('district');

        this.context.router.history.push('/login');
    }

    render() {
        return (
            <div className="app-sidebar sidebar-dark sidebar-slide-left">
                <div className="text-right">
                    <button type="button" className="btn btn-sidebar" data-dismiss="sidebar">
                        <span className="x"></span>
                    </button>
                </div>
                <div className="sidebar-header">
                    <img src="/assets/images/logo.png" alt="Logo" className="logo" />
                    <p className="appname">
                        PMU BOR <br />
                        <small>Settlement Application</small>
                        <br />
                        <small className="highlight-yellow"><b>Welcome {this.state.loggedUser}</b></small>
                    </p>
                </div>
                <div id="sidebar-nav" className="sidebar-nav" data-children=".sidebar-nav-group">
                    <Link to="/home" className="sidebar-nav-link">
                        <i className="fas fa-home"></i> Home
                        </Link>
                    <Link to="/progress" className={"sidebar-nav-link " + this.state.mProgress}>
                        <i className="fas fa-tasks"></i> Monthly Progress
                        </Link>
                    <Link to="/usummary" className={"sidebar-nav-link " + this.state.uSummary}>
                        <i className="fas fa-ban"></i> UnApproved List
                        </Link>
                    <Link to="/rsummary" className="sidebar-nav-link">
                        <i className="fas fa-stop-circle"></i>Rejected List
                    </Link >
                    <Link to="/monitoring" className="sidebar-nav-link">
                        <i className="fas fa-desktop"></i>Monitoring
                    </Link >

                    <div className={"sidebar-nav-group " + this.state.reports}>
                        <a href="#device-controls" className="sidebar-nav-link collapsed" data-toggle="collapse" data-parent="#sidebar-nav">
                            <i className="fas fa-tablet-alt"></i> Settlement Reports
                            </a>
                        <div id="device-controls" className="sidebar-nav-group collapse">
                            <Link to="/settlement/m" className={"sidebar-nav-link " + this.state.dMansehra}>
                                - Mansehra
                                </Link>
                            <Link to="/settlement/a" className={"sidebar-nav-link " + this.state.dAbbotabad}>
                                - Abbotabad
                                </Link>
                            <Link to="/settlement/c" className={"sidebar-nav-link " + this.state.dChitral}>
                                - Chitral
                                </Link>
                            <Link to="/settlement/n" className={"sidebar-nav-link " + this.state.dNoshera}>
                                - Noshera
                                </Link>
                        </div>
                    </div>
                    <a href="./index.html" className="sidebar-nav-link" onClick={this.logout}>
                        <i className="fas fa-sign-out-alt" ></i> Logout
                        </a>
                </div>
                <div className="sidebar-footer">
                </div>
            </div>
        )
    }
}
