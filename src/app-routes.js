import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Login from './components/login';
import Home from './components/home';
import Progress from './components/progress';
import Settlement from './components/settlement';
import USummary from './components/usummary';
import RSummary from './components/rsummary';
import UpdateProgress from './components/uprogress';
import { config } from './utils/configs';

export default class AppRoutes extends React.Component {

	//constructor(props) {
	//	super(props);
		//console.log(this.props);
        // initially assuming that user is logged out
		/*this.state = {
			isLoggedIn: true
		}*/

        // if user is logged in, his details can be found from local storage
        // updating the state
		/*this.state = {
			user: user
		};*/

		//this.authenticate = this.authenticate.bind(this);
	//}

	//componentWillMount() {
		/*try {
			let key = localStorage.getItem(config.localStorageKey);
			if (!key && key.length > 0) {
				this.setState(() => ({
					isLoggedIn: false
				}));
			}
		} catch (exception) {
		}*/
	//}

    // this function is called on login/logout
	/*authenticate(user) {
		this.setState({
			user: user
		});

		// updating user's details
		localStorage.setItem(config.localStorageKey, JSON.stringify(user));
	}*/

	

	render() {
		var key = localStorage.getItem(config.localStorageKey);
		var loggedIn = (key && key.length > 0) ? true : false;
		return (
			<Switch>
				<Route exact path='/' render={() => <Login />} />
				<Route path='/login' render={() => <Login />} />
			    
				<Route path="/home" render={() => (
			    	loggedIn ? 
			    	        (<Home />) : 
			    	        (<Login />)
			    )} /> 

				<Route path="/settlement" render={() => (
			    	loggedIn ? 
			    	        (<Settlement />) : 
			    	        (<Login />)
			    )} /> 

				<Route path="/progress" render={() => (
			    	loggedIn ? 
			    	        (<Progress />) : 
			    	        (<Login />)
			    )} />

				<Route path="/usummary" render={() => (
			    	loggedIn ? 
			    	        (<USummary />) : 
			    	        (<Login />)
			    )} />

				<Route path="/rsummary" render={() => (
			    	loggedIn ? 
			    	        (<RSummary />) : 
			    	        (<Login />)
			    )} />

				<Route path="/uprogress/:id" render={() => (
			    	loggedIn ? 
			    	        (<UpdateProgress {...this.props} />) : 
			    	        (<Login />)
			    )} />
			</Switch>
		);
	}
} 