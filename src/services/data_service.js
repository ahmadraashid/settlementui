import { config } from '../utils/configs';
require('isomorphic-fetch');

class DataService {

    constructor() {
        this.optionsGet = {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'withCredentials': true,
                'Content-Type': 'application/json',
                'Authorization': null
            }
        }

        this.optionsPostWithAuth = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'withCredentials': true,
                'Content-Type': 'application/json',
                'Authorization': null
            }
        }

        this.optionsPutWithAuth = {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'withCredentials': true,
                'Content-Type': 'application/json',
                'Authorization': null
            }
        }

        this.optionsPost = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'withCredentials': true,
                'Content-Type': 'application/json'
            }
        }

        this.authenticate = this.authenticate.bind(this);
    }

    setTokenForGet() {
        let token = 'Bearer ' + localStorage.getItem('token');
        this.optionsGet.headers.Authorization = token;
    }

    setTokenForPost() {
        let token = 'Bearer ' + localStorage.getItem('token');
        this.optionsPost.headers.Authorization = token;
    }

    setTokenForPostWithAuth() {
        let token = 'Bearer ' + localStorage.getItem('token');
        this.optionsPostWithAuth.headers.Authorization = token;
    }

    setTokenForPutWithAuth() {
        let token = 'Bearer ' + localStorage.getItem('token');
        this.optionsPutWithAuth.headers.Authorization = token;
    }

    getSummaryDetail(id) {
        this.setTokenForGet();
        var url = config.baseUrl + config.getProgressWithId + id;
        return fetch(url, this.optionsGet)
            .then((response) => { return response.json() });
    }

    getUnApprovedSummaries() {
        this.setTokenForGet();
        var url = config.baseUrl + config.getUnApprovedSummaries;
        return fetch(url, this.optionsGet)
            .then((response) => { return response.json() });
    }

    getRejectedSummaries() {
        this.setTokenForGet();
        var url = config.baseUrl + config.getRejectedSummaries;
        return fetch(url, this.optionsGet)
            .then((response) => { return response.json() });
    }

    getMozas() {
        this.setTokenForGet();
        var url = config.baseUrl + config.getUserMozas;
        return fetch(url, this.optionsGet)
            .then((response) => { return response.json() }); // change to return response.text()
    }

    authenticate(userNameVal, passwordVal) {
        this.setTokenForPost();
        this.optionsPost.body = JSON.stringify(
            {
                UserName: userNameVal,
                Password: passwordVal
            }
        );

        var url = config.baseUrl + config.getToken;
        return fetch(url, this.optionsPost)
        .then((response) => {
            if (!response.ok) {
                throw Error(response.statusText);
            }
            return response.json(); 
        }); // change to return response.text()
    }

    saveProgress(progress) {
        this.setTokenForPostWithAuth();
        this.optionsPostWithAuth.body = JSON.stringify(progress);
        var url = config.baseUrl + config.saveProgress;
        return fetch(url, this.optionsPostWithAuth)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response.json(); 
            });
    }

    saveVisit(visit) {
        this.setTokenForPostWithAuth();
        this.optionsPostWithAuth.body = JSON.stringify(progress);
        var url = config.baseUrl + config.saveVisit;
        return fetch(url, this.optionsPostWithAuth)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response.json(); 
            });
    }

    updateProgress(progress) {
        this.setTokenForPutWithAuth();
        this.optionsPutWithAuth.body = JSON.stringify(progress);
        var url = config.baseUrl + config.updateProgress + progress.Id;
        return fetch(url, this.optionsPutWithAuth)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response.json(); 
            });
    }

    approveSummaryStatus(updateSummary) {
        this.setTokenForPostWithAuth();
        this.optionsPostWithAuth.body = JSON.stringify(updateSummary);
        var url = config.baseUrl + config.approveProgress;
        return fetch(url, this.optionsPostWithAuth)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response.text(); 
            });
    }

    rejectSummaryStatus(updateSummary) {
        this.setTokenForPostWithAuth();
        this.optionsPostWithAuth.body = JSON.stringify(updateSummary);
        var url = config.baseUrl + config.rejectProgress;
        return fetch(url, this.optionsPostWithAuth)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response.text(); 
            });
    }

    signout() {
        return false;
    }
}

export default DataService;