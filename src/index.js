import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import AppRoutes from './app-routes';

const app = document.getElementById('app-body');

ReactDOM.render((
	<BrowserRouter>
	    <AppRoutes />
	</BrowserRouter>
), app);