import React from 'react';
import ReactRouter from 'react-router-dom';
import Navigation from './components/navigation';
import Login from './components/login';
import PrivateRoute from './components/login';

const Router = ReactRouter.Router;
const Route = ReactRouter.Route;
const IndexRoute = ReactRouter.IndexRoute;
const browserHistory = ReactRouter.browserHistory;

const Routes = {
    get: function (config) {
        return (
            <Router history={browserHistory}>
                <PrivateRoute path='/login' component={Login}></PrivateRoute>
                <Route path='/home' component={Home}></Route>
            </Router>
        );
    }
};

module.exports = Routes;

